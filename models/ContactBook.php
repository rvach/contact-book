<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_book".
 *
 * @property int $id
 * @property string $name
 * @property string $tel_number
 */
class ContactBook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'tel_number'], 'required'],
            [['name', 'tel_number'], 'string', 'max' => 125],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'tel_number' => 'Телефон',
        ];
    }
}
