<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Книга контактов</title>
    <!-- Подключаем шрифт Open Sans -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400&display=swap" rel="stylesheet">
    <!-- Подключаем Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>

        body {
            font-family: "Open Sans", sans-serif;
            background-color: #f0f0f0;
        }

        .container {
            background-color: #fff;
            padding: 30px;
            padding-bottom: 4%;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            max-width: 400px;
            margin: 1cm auto;
            margin-bottom: 30px;
            position: relative;
        }

        hr {
            border: none;
            border-top: 1px solid #eee;
            margin: 10px 0;
        }

        .form-group input[type="text"] {
            color: #555;
            background-color: #f9f9f9;
            border-color: #eee;
            border-radius: 5px;
            padding-left: 30px;
        }

        .form-group input[type="text"]:focus {
            outline: none;
            box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
        }

        .btn-add {
            background-color: #4d59a1;
            color: #fff;
            border-radius: 5px;
            position: absolute;
            right: 20px;
            bottom: 28px;
        }

        .contact-list {
            margin-top: 1cm;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
            padding: 30px;
            border-radius: 5px;
            max-width: 400px;
        }

        .contact-item {
            padding: 10px 0;
            position: relative;
            font-size: 12.36pt;
        }

        .contact-item hr {
            margin: 10px 0;
        }

        .contact-item .delete-contact {
            color: #000;
            position: absolute;
            right: 10px;
            top: 50%;
            transform: translateY(-50%);
            cursor: pointer;
        }

        .contact-item .phone {
            font-size: 10.88pt;
        }

        .container h2 {
            font-size: 12.36pt;
            margin-top: 0;
            padding-bottom: 13px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="text-left">
        <h2>Добавить контакт</h2>
        <hr>
    </div>
    <form class="form">
        <div class="form-group">
            <input type="text" class="form-control" id="name" required placeholder="Имя">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="number" required placeholder="Телефон" oninput="this.value = this.value.replace(/[^\d+]/g, '');">
        </div>
        <button type="submit" class="btn btn-add">Добавить</button>
    </form>
</div>
<div class="container contact-list">
    <div class="text-left">
        <h2>Список контактов</h2>
        <hr>
    </div>
    <?php
    if ($contacts) {
        foreach ($contacts as $c) {
            ?>
            <div class="contact-item">
                <?= $c->name ?>
                <br>
                <span class="phone"><?= $c->tel_number ?></span>
                <hr>
                <span class="delete-contact" data-id="<?= $c->id ?>">✕</span>
            </div>
            <?php
        }
    }
    ?>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(document).on("click", ".btn-add", function (event) {
        event.preventDefault();
        var name = $("#name").val();
        var number = $("#number").val();

        $.ajax({
            type: 'GET',
            url: "<?= \yii\helpers\Url::to(['contact-book/create']) ?>",
            dataType: 'json',
            data: {
                name: name,
                number: number
            },
            success: function (response) {
                location.reload();
            },
            error: function (xhr, status, error) {
                // Обработка ошибки
                console.log(xhr.responseText);
            }
        });

        $("#name").val("");
        $("#number").val("");
    });

    $(document).on("click", ".delete-contact", function () {
        var contactId = $(this).data("id");

        $.ajax({
            type: 'GET',
            url: "<?= \yii\helpers\Url::to(['contact-book/del']) ?>",
            dataType: 'json',
            data: {
                id: contactId
            },
            success: function (response) {
                if (response.success) {

                    location.reload();
                } else {
                    console.log(response.message);
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
</script>
</body>
</html>