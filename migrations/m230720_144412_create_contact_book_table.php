<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact_book}}`.
 */
class m230720_144412_create_contact_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%contact_book}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(125)->notNull(),
            'tel_number' => $this->string(125)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%contact_book}}');
    }
}
