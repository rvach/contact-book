<?php

namespace app\controllers;

use app\models\ContactBook;
use Yii;
use yii\web\Controller;

/**
 * ContactBookController implements the CRUD actions for ContactBook model.
 */
class ContactBookController extends Controller
{

    public function actionCreate()
    {
        $name = Yii::$app->request->get('name');
        $number = Yii::$app->request->get('number');

        $contact = new ContactBook();

        $contact->name = $name;
        $contact->tel_number = $number;
        if ($contact->save()) {
            return json_encode(['success' => true, 'message' => 'Контакт успешно добавлен']);
        } else {
            return json_encode(['success' => false, 'message' => 'Ошибка при добавлении контакта']);
        }

    }

    public function actionDel($id)
    {
        $contact = ContactBook::find()
            ->where(['id' => $id])
            ->one();

        if ($contact->delete()) {
            return json_encode(['success' => true, 'message' => 'Контакт успешно удален']);
        } else {
            return json_encode(['success' => false, 'message' => 'Ошибка при удалении контакта']);
        }
    }

}
